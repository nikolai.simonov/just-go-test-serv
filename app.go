package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"os"
	"net/http/httptest"
	"net/http/httputil"
	"github.com/gorilla/handlers"
)

type Task struct {
	Id    int    `json:"id"`
	Title string `json:"label"`
	IsDone   bool `json:"isDone"`
}

func main() {
	openLogFile("server.log")

	log.SetFlags(log.Ldate | log.Ltime )

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", logHandler(Index))
	router.HandleFunc("/todos", logHandler(TodoIndex))

	loggedRouter := handlers.CombinedLoggingHandler(os.Stdout, router)
	http.ListenAndServe(":80", loggedRouter)
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome!")
}

func TodoIndex(w http.ResponseWriter, r *http.Request) {
	var u = r.Header.Get("user")
	if r.Method == "POST" {

		decoder := json.NewDecoder(r.Body)
		var t []Task
		err := decoder.Decode(&t)
		if err != nil {
			panic(err)
		}

		writeToFile(u, t)
		return
	}

	var rt = readTasks(u)

	b, _ := json.Marshal(&rt)

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}


func writeToFile(k string, t []Task) {
	b, _ := json.Marshal(&t)
	err := ioutil.WriteFile("./"+k+".json", b, 0644)
	if err != nil {
		panic(err)
	}
}


func readTasks(k string) []Task {
	raw, err := ioutil.ReadFile("./"+k+".json")
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	var c []Task
	json.Unmarshal(raw, &c)
	return c
}

func openLogFile(logfile string) {
	if logfile != "" {
		lf, err := os.OpenFile(logfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0640)

		if err != nil {
			log.Fatal("OpenLogfile: os.OpenFile:", err)
		}

		log.SetOutput(lf)
	}
}


func logHandler(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		x, err := httputil.DumpRequest(r, true)
		if err != nil {
			http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
			return
		}
		log.Println(fmt.Sprintf("%q", x))
		rec := httptest.NewRecorder()
		fn(rec, r)
		log.Println(fmt.Sprintf("%q", rec.Body))

		// this copies the recorded response to the response writer
		for k, v := range rec.HeaderMap {
			w.Header()[k] = v
		}
		w.WriteHeader(rec.Code)
		rec.Body.WriteTo(w)
	}
}
